# **Refactor game to include a HumanPlayer class**

# Before actually implementing the computer player, let's make things easier on ourselves and refactor the game to accept player classes. In order to accomplish this, move all of your user input logic into the player class. I moved the following methods out of the Game class into the player:

# * `prompt`
# * `get_input`

class HumanPlayer
	attr_accessor :previous_guess

	def initialize(_size)
		@previous_guess = nil 
	end

	def prompt_player
		puts "Please enter the position of the card you would like to flip (e.g., '2,3')"
	end

	def get_guess
		prompt_player
		guess = gets.chomp.split(",")
		guess.map(&:to_i)
	end

	def receive_match(_match1, _match2)
	end

	def receive_revealed_card(position, value)
		# duck typing
	end
end