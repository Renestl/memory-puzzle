# The Game should have instance variable for the Board and the previously-guessed position (if any). It should also have methods for managing the Board-Player interaction. You may want a play loop that runs until the game is over. Inside the loop, you should render the board, prompt the player for input, and get a guessed pos. Pass this pos to a make_guess method, where you will handle the actual memory/matching logic. Some tips on implementing this:

# If we're not already checking another Card, leave the card at guessed_pos face-up and update the previous_guess variable.
# If we are checking another card, we should compare the Card at guessed_pos with the one at previous_guess.
# If the cards match, we should leave them both face-up.
# Else, flip both cards face-down.
# In either case, reset previous_guess.

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
	attr_reader :player, :size

	def initialize(player, size = 4)
		@board = Board.new(size)
		@previous_guess = nil
		@player = player
	end

	def play
		until board.won?
			board.render		
			guess_pos = get_player_guess
			make_guess(guess_pos)
		end

		puts "You won!"
	end

	def get_player_guess
		guess_pos = nil
		
		until guess_pos && valid_guess(guess_pos)
			guess_pos = player.get_guess
		end

		guess_pos
	end

	def compare_guess(current_guess)
		if previous_guess
			if match?(previous_guess, current_guess)
				player.receive_match(previous_guess, current_guess)
				puts "It's a match!"
			else
				puts "Try again."
				[previous_guess, current_guess].each { |pos| board[pos].hide}		
			end
			@previous_guess = nil
			player.previous_guess = nil
		else
			@previous_guess = current_guess
			player.previous_guess = current_guess
		end
	end

	def make_guess(guess)
		current_guess = board.reveal(guess)
		player.receive_revealed_card(guess, current_guess)
		board.render

		compare_guess(guess)

		sleep(1)
		board.render
	end

	def match?(position1, position2)
		@board[position1] == @board[position2]
	end

	def valid_guess(position)
		position.is_a?(Array) && 
			position.count == 2 && 
			position.all? { |x| x.between?(0, board.size - 1) }
	end

	private

  attr_accessor :previous_guess
	attr_reader :board
end

if __FILE__ == $PROGRAM_NAME
	# Game.new(ComputerPlayer.new(size), size)
	# # game.play
end