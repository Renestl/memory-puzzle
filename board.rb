# The Board is responsible for keeping track of all your Cards. You'll want a grid instance variable to contain Cards. Useful methods:

# `#populate` should fill the board with a set of shuffled Card pairs
# `#render` should print out a representation of the Board's current state
# `#won?` should return true if all cards have been revealed.
# `#reveal` should reveal a Card at `guessed_pos` (unless it's already face-up, in which case the method should do nothing). It should also return the value of the card it revealed (you'll see why later).

require_relative 'card'

class Board
	attr_reader :size

	def initialize(size = 4)
		@grid = Array.new(size) { Array.new(size) }
		@size = size
		populate
	end

	def populate
		num_pairs = (size**2) / 2
		cards = Card.shuffle(num_pairs)

		@grid.each_index do |row|
			grid[row].each_index do |col|
				grid[row][col] = cards.pop
			end
		end
	end

	def reveal(guessed_position)
		if revealed?(guessed_position)
			puts "This card is currently revealed."
		else
			self[guessed_position].reveal
		end
		self[guessed_position].face_value
	end

	def hide(position)
		self[position].hide
	end

	def render
		system("clear")
		puts "  #{(0...@size).to_a.join(" ")}"
		grid.each_with_index do |row, index|
			print "#{index} "
			puts row.map(&:display).join(" ")
		end
	end

	def revealed?(position)
		self[position].is_revealed?
	end

	def won?
		grid.all? do |row| 
			row.all?(&:is_revealed?)
		end
	end

	def [](grid_position)
		row, col = grid_position
		grid[row][col]
	end

	def []=(position, value)
		row, col = position
		grid[row][col] = value
	end

	private

  attr_reader :grid
end