# The computer player's strategy should be as follows:

# * On its first guess, if it knows where 2 matching cards are, guess one of them, otherwise guess randomly among cards it has not yet seen.
# * On its second guess, if its first guess revealed a card whose value matches a known location, guess that location, otherwise guess randomly among cards it has not yet seen.

# Now comes the tricky part. With the HumanPlayer, we didn't need to explicitly receive the data from the card we're flipping over; we just read it off the terminal output. The computer won't be quite so savvy.

class ComputerPlayer
	attr_accessor :board_size, :previous_guess

	def initialize(size)
		@board_size = size
		@known_cards = {}
		@matched_cards = {}
		@previous_guess = nil
	end

	def get_guess
		if previous_guess
			second_guess
		else
			first_guess
		end
	end

	def receive_revealed_card(position, value)
		@known_cards[position] = value
	end

	def receive_match(match1, match2)
		@matched_cards[match1] = true
		@matched_cards[match2] = true
	end

	def first_guess
		matching_guess? || random_guess
	end

	def second_guess
		match_known || random_guess
	end

	def matching_guess?
		if !@known_cards.empty?
			match = @known_cards.find do |position, value|
				position != @previous_guess && value == @known_cards[position] && !@matched_cards[position]	
			end
			match[0]
		end
	end

	def match_known
		guess = @known_cards.find do |position, value|
			position != @previous_guess && value == @known_cards[@previous_guess] && !@matched_cards[position]
		end

		guess
	end

	def random_guess
		guess = nil 

		until guess && !@known_cards[guess] && !@matched_cards[guess]
			guess = [rand(board_size), rand(board_size)]
		end

		guess
	end
end