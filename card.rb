# A Card has two useful bits of information: its face value, and whether it is face-up or face-down. You'll want instance variables to keep track of this information. You'll also want a method to display information about the card: nothing when face-down, or its value when face-up. I also wrote `#hide`, `#reveal`, `#to_s`, and `#==` methods.

class Card
	LETTERS = ("A"..."Z").to_a.shuffle

	def self.shuffle(num_pairs)
		letters = LETTERS

		while num_pairs > letters.length
			letters = letters + letters
		end

		card_array = letters.take(num_pairs) * 2
		card_array.shuffle!
		cards = card_array.map { |card| self.new(card)}
	end

	attr_reader :face_value
	
	def initialize(face_value, revealed = false)
		@face_value = face_value
		@revealed = revealed
	end

	def hide
		@revealed = false
	end

	def reveal
		@revealed = true
	end

	def is_revealed?
		@revealed
	end

	def display
		is_revealed? ? face_value.to_s :	" "
	end

	# to compare 2 diff objects that have the same face_value
	def ==(other_card)
		other_card.is_a?(self.class) && other_card.face_value == face_value
	end
end