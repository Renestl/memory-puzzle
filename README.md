# Memory Puzzle
Implementation in Ruby for [App Academy Open](https://open.appacademy.io/)

Based on the classic [Match 2 memory card game](http://mypuzzle.org/find-the-pair)

## Learning Goals

* Understand how classes interact in an object-oriented program
* Be able to use `require_relative`
* Be able to write the methods `[]` and `[]=` and explain how they work
* Develop a workflow that uses pry to test small parts of your code
* Know how to initialize an Array with a default value
* Know how to use duck typing to allow different classes to interact with your program

Write classes for Card, Board, and Game. Please put each class in its own file and use `require_relative` to include other files in your program.
